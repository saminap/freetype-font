#pragma once

#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_WRITE_STATIC
extern "C" {
#include "stb_image_write.h"
}

#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <algorithm>
#include <fcntl.h>
#include <unistd.h>
#include <iomanip>
#include <cstring>
#include <sstream>
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
#include FT_BITMAP_H
#include FT_STROKER_H
#include FT_IMAGE_H

namespace FTFont
{
typedef struct
{
	int x;
	int y;
	int w;
	int h;
	int advance;
	int bearing;
}Char;

typedef struct
{
	std::shared_ptr<unsigned char> data;
	int size;
}Buffer;

class Font
{
public:
	Font(const std::string& filename,
	     int dpi=DEFAULT_DPI,
	     int baseFontSize=BASE_FONT_SIZE,
	     int borderLevelsToGenerate=DEFAULT_BORDER_LEVELS);
	Font(const unsigned char* buffer,
	     size_t bufferSize,
	     int dpi=DEFAULT_DPI,
	     int baseFontSize=BASE_FONT_SIZE,
	     int borderLevelsToGenerate=DEFAULT_BORDER_LEVELS);
	Font(const unsigned char* buffer,
	     size_t bufferSize,
	     std::vector<std::map<char, Char>> (*deserializerFunc)(const unsigned char*, size_t));
	const Char* getChar(char c, int border=0) const;
	int getMaxCharHeight(int border=0) const;
	const unsigned char* getAtlasBuffer() const;
	Buffer getPngBuffer();
	size_t getAtlasBufferSize() const;
	size_t getAtlasWidth() const;
	size_t getAtlasHeight() const;
	void releaseBitmapBuffer();
	void translateToRGBA();
	void printBitmap() const;
	const std::string& getFontName() const;
	void setPngBuffer(void* data, int size);
	void writeToFile(const std::string& filename);
	void serializeToFile(const std::string& filename,
	                     Buffer (*serializationFunc)
	                            (const std::vector<std::map<char, Char>>&)=defaultSerializer);
	Buffer serialize(Buffer (*serializationFunc)
	                        (const std::vector<std::map<char, Char>>&)=defaultSerializer);

	static void stbiCallbackFunc(void* context, void* data, int size);
	static Buffer defaultSerializer(const std::vector<std::map<char, Char>>& charMap);
	static std::vector<std::map<char, Char>> defaultDeserializer(const unsigned char* buffer,
	                                                             size_t bufferSize);

	static const int BASE_FONT_SIZE = 16;
	static const int DEFAULT_BORDER_LEVELS = 5;
	static const int CHARACTER_SPACING = 3;
	static const int DEFAULT_DPI = 72;
	static const int ASCII_START=32;
	static const int ASCII_END=126;
	static const int BITMAP_BYTE_ALIGNMENT=4;

private:
	std::unique_ptr<unsigned char> atlasBuffer;
	size_t atlasWidth=0;
	size_t atlasHeight=0;
	std::vector<std::map<char, Char>> characters;
	std::vector<int> maxCharHeights;
	bool rgba=false;
	std::string fontName;
	Buffer pngBuffer;

	void init(const unsigned char* buffer,
	          size_t size,
	          int dpi,
	          int baseFontSize,
	          int borderLevelsToGenerate);
	void createAtlasBuffer(std::unique_ptr<unsigned char>& atlas,
	                       const std::vector<std::map<char, FT_Glyph>>& glyphMap,
	                       const std::vector<int>& maxBearings,
	                       size_t& width,
	                       size_t& height);
	void populateAtlasBuffer(std::unique_ptr<unsigned char>& atlas,
	                         const std::vector<std::map<char, FT_Glyph>>& glyphMap,
	                         const std::vector<int>& maxBearings,
	                         size_t width,
	                         size_t height);
	void calculateMaxCharHeights();
};
}
