#include "freetype_font.hpp"

namespace FTFont
{
Font::Font(const std::string& filename,
           int dpi,
           int baseFontSize,
           int borderLevelsToGenerate)
{
	int fd = open(filename.c_str(), O_RDONLY);
	std::unique_ptr<unsigned char> buffer;
	size_t size = 0;

	if(fd < 0) // Could return an error message
		return;

	size = lseek(fd, 0, SEEK_END);
	buffer = std::unique_ptr<unsigned char>(new unsigned char[size]);
	lseek(fd, 0, SEEK_SET);
	read(fd, buffer.get(), size);
	close(fd);

	init(buffer.get(), size, dpi, baseFontSize, borderLevelsToGenerate);
}

Font::Font(const unsigned char* buffer,
           size_t bufferSize,
           int dpi,
           int baseFontSize,
           int borderLevelsToGenerate)
{
	init(buffer, bufferSize, dpi, baseFontSize, borderLevelsToGenerate);
}

Font::Font(const unsigned char* buffer,
           size_t bufferSize,
           std::vector<std::map<char, Char>> (*deserializerFunc)(const unsigned char*, size_t))
{
	characters = deserializerFunc(buffer, bufferSize);
	Char lastChar = characters.back().rbegin()->second;
	atlasHeight = 0;
	atlasWidth = lastChar.x+lastChar.w+CHARACTER_SPACING;

	int modulo = atlasWidth % BITMAP_BYTE_ALIGNMENT;
	if(modulo != 0)
		atlasWidth += BITMAP_BYTE_ALIGNMENT-modulo;

	for(auto iter : characters.back())
	{
		Char character = iter.second;
		atlasHeight = std::max((int)atlasHeight, character.y+character.h+CHARACTER_SPACING);
	}

	calculateMaxCharHeights();
}

void Font::init(const unsigned char* buffer,
                size_t size,
                int dpi,
                int baseFontSize,
                int borderLevelsToGenerate)
{
	FT_Library lib;
	FT_Face face;
	FT_Stroker stroker;
	FT_UInt index;
	size_t width = 0, height = 0;
	std::vector<std::map<char, FT_Glyph>> glyphMap;
	std::vector<int> maxBearings;

	if(FT_Init_FreeType(&lib))
		return;

	if(FT_New_Memory_Face(lib, buffer, size, 0, &face))
		return;

	if(FT_Set_Char_Size(face, baseFontSize*64, 0, dpi, 0))
		return;

	if(FT_Stroker_New(lib, &stroker))
		return;

	fontName = face->family_name;

	for(int border=0; border<=borderLevelsToGenerate; ++border)
	{
		int maxBearing = 0;

		FT_Stroker_Set(stroker, border*baseFontSize, FT_STROKER_LINECAP_ROUND, FT_STROKER_LINEJOIN_ROUND, 0);
		glyphMap.push_back({});

		for(int c=ASCII_START; c<=ASCII_END; ++c)
		{
			FT_Glyph glyph;
			index = FT_Get_Char_Index(face, c);
			FT_Load_Glyph(face, index, FT_LOAD_DEFAULT);
			FT_Get_Glyph(face->glyph, &glyph);
			FT_Glyph_StrokeBorder(&glyph, stroker, false, true);
			FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, 0);
			FT_BitmapGlyph bitmapGlyph = (FT_BitmapGlyph)glyph;

			maxBearing = std::max(maxBearing, bitmapGlyph->top);

			glyphMap.at(border)[(char)c] = glyph;
		}

		maxBearings.push_back(maxBearing);
	}

	createAtlasBuffer(atlasBuffer, glyphMap, maxBearings, width, height);
	populateAtlasBuffer(atlasBuffer, glyphMap, maxBearings, width, height);
	calculateMaxCharHeights();

	atlasWidth = width;
	atlasHeight = height;

	for(auto& borderLevel : glyphMap)
	{
		for(auto& glyph : borderLevel)
		{
			FT_Done_Glyph(glyph.second);
		}
	}

	FT_Stroker_Done(stroker);
	FT_Done_Face(face);
	FT_Done_FreeType(lib);
}

void Font::createAtlasBuffer(std::unique_ptr<unsigned char>& atlas,
                             const std::vector<std::map<char, FT_Glyph>>& glyphMap,
                             const std::vector<int>& maxBearings,
                             size_t& width,
                             size_t& height)
{
	std::vector<std::map<char, FT_Glyph>>::const_iterator borderIter = glyphMap.begin();
	std::vector<int>::const_iterator bearingIter = maxBearings.begin();

	width = 0;
	height = 0;

	for(; borderIter != glyphMap.end(); ++borderIter)
	{
		int rowHeight = 0;
		for(auto charIter=borderIter->begin(); charIter != borderIter->end(); ++charIter)
		{
			FT_BitmapGlyph bitmapGlyph = (FT_BitmapGlyph)charIter->second;

			int diffToMaxBearing = *bearingIter-bitmapGlyph->top;
			int charHeight = diffToMaxBearing+bitmapGlyph->bitmap.rows+CHARACTER_SPACING;
			rowHeight = std::max(rowHeight, charHeight);

			// If we are looping the last border level, then use that data
			// to calculate the atlas width
			if(borderIter == glyphMap.end()-1)
				width += bitmapGlyph->bitmap.width+CHARACTER_SPACING;
		}

		height += rowHeight;
		++bearingIter;
	}

	int modulo = width % BITMAP_BYTE_ALIGNMENT;
	if(modulo != 0)
		width += BITMAP_BYTE_ALIGNMENT-modulo;

	atlas.reset(new unsigned char[height*width]);
	std::fill_n(atlas.get(), height*width, 0);
}

void Font::populateAtlasBuffer(std::unique_ptr<unsigned char>& atlas,
                               const std::vector<std::map<char, FT_Glyph>>& glyphMap,
                               const std::vector<int>& maxBearings,
                               size_t width,
                               size_t height)
{
	unsigned char* buffer = atlas.get();
	int xOffset = 0, yOffset = 0;
	std::vector<std::map<char, FT_Glyph>>::const_iterator borderIter = glyphMap.begin();
	std::vector<int>::const_iterator bearingIter = maxBearings.begin();

	for(; borderIter != glyphMap.end(); ++borderIter, ++bearingIter)
	{
		int rowHeight = 0;
		characters.push_back({});
		for(auto charIter=borderIter->begin(); charIter != borderIter->end(); ++charIter)
		{
			FT_BitmapGlyph bitmapGlyph = (FT_BitmapGlyph)charIter->second;
			int diffToMaxBearing = *bearingIter-bitmapGlyph->top;
			int yPosition = diffToMaxBearing+yOffset;
			int charHeight = diffToMaxBearing+bitmapGlyph->bitmap.rows+CHARACTER_SPACING;
			int xPosition = xOffset;

			for(int y=0; y<bitmapGlyph->bitmap.rows; ++y)
			{
				for(int x=0; x<bitmapGlyph->bitmap.width; ++x)
				{
					buffer[yPosition*width+xPosition] = bitmapGlyph->bitmap.buffer[y*bitmapGlyph->bitmap.pitch+x];
					++xPosition;
				}
				++yPosition;
				xPosition = xOffset;
			}

			// Populate the coordinates and other glyph related data
			characters.at(std::distance(glyphMap.begin(), borderIter))[charIter->first] = {
				xOffset,
				yOffset,
				(int)bitmapGlyph->bitmap.width,
				charHeight-CHARACTER_SPACING,
				(int)(charIter->second->advance.x >> 16),
				bitmapGlyph->left
			};

			xOffset += bitmapGlyph->bitmap.width + CHARACTER_SPACING;
			rowHeight = std::max(rowHeight, charHeight);
		}

		xOffset = 0;
		yOffset += rowHeight;
	}
}

void Font::calculateMaxCharHeights()
{
	for(auto borderLevel : characters)
	{
		int height = 0;

		for(auto c : borderLevel)
			height = std::max(height, c.second.h);

		maxCharHeights.push_back(height);
	}
}

const Char* Font::getChar(char c, int border) const
{
	if(border >= characters.size() || c < ASCII_START || c > ASCII_END)
		return nullptr;

	return &characters.at(border).find(c)->second;
}

int Font::getMaxCharHeight(int border) const
{
	if(border >= maxCharHeights.size() || border < 0)
		return 0;

	return maxCharHeights.at(border);
}

const unsigned char* Font::getAtlasBuffer() const
{
	return atlasBuffer.get();
}

Buffer Font::getPngBuffer()
{
	if(!pngBuffer.data && atlasBuffer)
	{
		stbi_write_png_to_func(Font::stbiCallbackFunc,
		                       this,
		                       atlasWidth,
		                       atlasHeight,
		                       4,
		                       atlasBuffer.get(),
		                       atlasWidth*4);
	}

	return pngBuffer;
}

size_t Font::getAtlasBufferSize() const
{
	return rgba ?
	         getAtlasWidth()*getAtlasHeight()*4 :
	         getAtlasWidth()*getAtlasHeight();
}

size_t Font::getAtlasWidth() const
{
	return atlasWidth;
}

size_t Font::getAtlasHeight() const
{
	return atlasHeight;
}

void Font::releaseBitmapBuffer()
{
	atlasBuffer.reset();
	pngBuffer.data.reset();
}

void Font::translateToRGBA()
{
	// Let's do the translation only once
	if(rgba)
		return;

	size_t newWidth = getAtlasWidth()*4;
	size_t origSize = getAtlasBufferSize();
	unsigned char* buffer = new unsigned char[newWidth*getAtlasHeight()]{0};
	unsigned char* src = atlasBuffer.get();
	const Char* firstBorder = getChar(ASCII_START, 1);
	int greenChannelThreshold = firstBorder ? firstBorder->y*getAtlasWidth() : origSize;
	int channel = 0;

	for(int i=0; i<origSize; ++i)
	{
		if(!channel && i >= greenChannelThreshold)
			++channel;

		if(src[i] > 0)
		{
			buffer[i*4+channel] = src[i];
			buffer[i*4+3] = src[i];
		}
	}

	rgba = true;
	atlasBuffer.reset(buffer);
}

void Font::printBitmap() const
{
	size_t width = rgba ? getAtlasWidth()*4 : getAtlasWidth();

	std::cout << "Bitmap width=" << getAtlasWidth() << " height=" << getAtlasHeight() << std::endl;

	for(int y=0;y<getAtlasHeight(); ++y)
	{
		for(int x=0; x<width; ++x)
		{
			std::cout << std::setw(3) << (int)atlasBuffer.get()[y*width+x] << " ";
		}

		std::cout << std::endl;
	}
}

const std::string& Font::getFontName() const
{
	return fontName;
}

void Font::stbiCallbackFunc(void* context, void* data, int size)
{
	Font* self = (Font*)context;
	self->setPngBuffer(data, size);
}

void Font::setPngBuffer(void* data, int size)
{
	pngBuffer.data = std::shared_ptr<unsigned char>(new unsigned char[size]);
	unsigned char* buffer = pngBuffer.data.get();

	for(int i=0; i<size; ++i)
		buffer[i] = ((unsigned char*)data)[i];

	pngBuffer.size = size;
}

void Font::writeToFile(const std::string& filename)
{
	FILE* f = fopen(filename.c_str(), "wb");
	bool cleanup = pngBuffer.data == nullptr;
	Buffer png = getPngBuffer();
	fwrite(png.data.get(), 1, png.size, f);
	fclose(f);

	if(cleanup)
		pngBuffer.data.reset();
}

void Font::serializeToFile(const std::string& filename,
                           Buffer (*serializationFunc)(const std::vector<std::map<char, Char>>&))
{
	FILE* f = fopen(filename.c_str(), "wb");
	Buffer buffer = serialize(serializationFunc);
	fwrite(buffer.data.get(), 1, buffer.size, f);
	fclose(f);
}

Buffer Font::serialize(Buffer (*serializationFunc)(const std::vector<std::map<char, Char>>&))
{
	return serializationFunc(characters);
}

Buffer Font::defaultSerializer(const std::vector<std::map<char, Char>>& charMap)
{
	Buffer buffer{};
	std::stringstream stream;
	for(const std::map<char, Char>& map : charMap)
	{
		for(const auto& iter : map)
		{
			stream << iter.first << ","
			       << iter.second.x << ","
			       << iter.second.y << ","
			       << iter.second.w << ","
			       << iter.second.h << ","
			       << iter.second.advance << ","
			       << iter.second.bearing << std::endl;
		}
	}

	buffer.size = stream.str().size();
	buffer.data = std::shared_ptr<unsigned char>(new unsigned char[buffer.size]);
	std::memcpy(buffer.data.get(), stream.str().c_str(), buffer.size);

	return buffer;
}

std::vector<std::map<char, Char>> Font::defaultDeserializer(const unsigned char* buffer,
                                                            size_t bufferSize)
{
	std::vector<std::map<char, Char>> map;
	std::istringstream stream(std::string((const char*)buffer, bufferSize));
	std::string line, value;
	int borderIndex = -1; // Increment happens at the very beginning

	while(std::getline(stream, line))
	{
		if(line.at(0) == ASCII_START)
		{
			++borderIndex;
			map.push_back({});
		}

		std::istringstream lineStream(line);
		std::vector<std::string> values;

		while(std::getline(lineStream, value, ','))
		{
			values.push_back(value);
		}

		if(values.size() == 7)
		{
			map.at(borderIndex)[values.at(0).at(0)] = Char{
				std::stoi(values.at(1)),
				std::stoi(values.at(2)),
				std::stoi(values.at(3)),
				std::stoi(values.at(4)),
				std::stoi(values.at(5)),
				std::stoi(values.at(6))
			};
		}
	}

	return map;
}
}
